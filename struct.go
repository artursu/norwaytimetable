package timetable

type departures struct {

  Data data `json:data`

}

type data struct {

  StopPlace stopPlace `json:stopPlace`

}

type stopPlace struct {

  Name string `json:name`
  EstimatedCalls []call `json:estimatedCalls`

}

type call struct {

  AimedDepartureTime string `json:aimedDepartureTime`
  DestinationDisplay destinationDisplay `json:destinationDisplay`
  ServiceJourney serviceJourney `json:serviceJourney`

}

type destinationDisplay struct {

  FrontText string `json:frontText`

}

type serviceJourney struct {

  JourneyPattern journeyPattern `json:journeyPattern`

}

type journeyPattern struct {

  Line line `json:line`

}

type line struct {

  Name string `json:name`

}
