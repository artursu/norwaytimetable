package main

import (

  "fmt"
  "timetable"
  "os"
  "log"
  "net/http"

)

func main() {

  //Gets port and checks if it's "80", if not port is set to be "80"
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
		fmt.Println("$PORT not found! Setting to 8080")
	}

	//HandleFunc() for each endpoint
	http.HandleFunc("/", timetable.DefaultHandler)
	http.HandleFunc("/Gjoevik/", timetable.GjoevikHandler)

	//Prints which port server is listening on
	fmt.Println("Listening on port " + port)

	//Initialises server
	log.Fatal(http.ListenAndServe(":"+port, nil))


}
